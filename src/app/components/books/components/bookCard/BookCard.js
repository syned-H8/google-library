import './bookCard.css';

import React from 'react';

const BookCard = props => {
  return (
    <div className="book-card">
      <h2 className="book-card__title">{props.title}</h2>
      <h3 className="book-card__subtitle">{props.subtitle}</h3>
      <div className="book-card__wrapper">
        <img
          src={props.image}
          alt="Book title"
          className="book-card__image"
        />
        <div className="book-card__info">
          <p className="book-card__desc">{props.desc}</p>
          <p className="book-card__publishedDate">{props.publishedDate}</p>
          <p>
            {props.authors.map((author, i) => {
              if (i === props.authors.length - 1) {
                return (
                  <li key={i}>{author}</li>
                )
              } else {
                return (
                  <li key={i}>{author},&nbsp;</li>
                )
              }
            })}
          </p>
          {props.myList ? (
            <button
              onClick={() => props.handleRemoveMyBook(props)}
              className="book-card__btn">
              Remove
            </button>
          ) : (
            <button
              onClick={() => props.handleAddMyBook(props)}
              className="book-card__btn">
              Add to My List
            </button>
          )}
        </div>
      </div>
    </div>
  )
}

export default BookCard;
