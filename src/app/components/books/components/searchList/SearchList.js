import './searchList.css';

import React from 'react';
import BookCard from '../bookCard';

const ResultBooks = (props) => {
  if (props.books.length === 0) {
    return <div></div>;
  } else {
    return (
      <div className="result-book">
        <h1 className="result-book__header">Search Result:</h1>
        <div className="result-book-list">
          {
            props.books.map((book, i) => {
              return (
                <BookCard
                  key={i}
                  title={book.volumeInfo.title.substring(0, 80)}
                  subtitle={book.volumeInfo.subtitle || 'no subtitle'}
                  image={book.volumeInfo.imageLinks
                    ? book.volumeInfo.imageLinks.smallThumbnail
                    : 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/1024px-No_image_available.svg.png'
                  }
                  desc={book.volumeInfo.description
                    ? `${book.volumeInfo.description.substring(0, 200)}...`
                    : 'no description'
                  }
                  publishedDate={book.volumeInfo.publishedDate}
                  authors={book.volumeInfo.authors || []}
                  handleAddMyBook={props.handleAddMyBook}
                />
              )
            })
          }
        </div>
      </div>
    )
  }
}

export default ResultBooks;