import './myList.css';

import React from 'react';
import BookCard from '../bookCard';

const MyBooks = (props) => {
  if (props.myBooks.length === 0) {
    return <div></div>;
  } else {
    return (
      <div className="my-books">
        <h1 className="my-books__header">My Books: </h1>
        <div className="my-books-list">
          {
            props.myBooks.map((book, i) => {
              return (
                <BookCard
                  key={`my-book ${i}`}
                  title={book.title}
                  subtitle={book.subtitle}
                  image={book.image}
                  desc={book.desc}
                  publishedDate={book.publishedDate}
                  authors={book.authors}
                  handleRemoveMyBook={props.handleRemoveMyBook}
                  myList={true}
                />
              )
            })
          }
        </div>
      </div>
    )
  }
}

export default MyBooks;