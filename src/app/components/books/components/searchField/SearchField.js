import React from 'react';
import './searchField.css';

const SearchField = (props) => {
  return (
    <form
      className="search-form"
      action="/"
      onSubmit={props.handleSearchSubmit}
    >
      <div className="search-form__wrapper">
        <input
          type="text"
          name="name"
          className="search-form__field"
          onChange={props.handleSearch} />
        <input type="submit" value="Find" className="search-form__submit" />
      </div>
    </form>
  )
}

export default SearchField;
