import './Books.css';

import React, { useState } from 'react';
import SearchField from './components/searchField';
import SearchList from './components/searchList';
import MyList from './components/myList';


const Books = () => {
  const [userInput, setUserInput] = useState('');
  const [books, setBooks] = useState([]);
  const [myBooks, setMyBooks] = useState([]);

  const handleSearch = e => {
    setUserInput(e.target.value);
  }

  const handleSearchSubmit = e => {
    e.preventDefault();

    fetch(`https://www.googleapis.com/books/v1/volumes?q=${userInput}`)
      .then(res => res.json())
      .then(
        result => {
          setBooks([...result.items]);
        },
      )
  }

  const handleAddMyBook = (book) => {
    setMyBooks([...myBooks, book]);

    setBooks(books.filter(el => {
      return el.volumeInfo.publishedDate !== book.publishedDate && el.volumeInfo.authors !== book.authors
    }));
  }

  const handleRemoveMyBook = (book) => {
    setMyBooks(myBooks.filter(el => {
      return el.title !== book.title && el.authors !== book.authors
    }));
  }

  return (
    <div>
      <SearchField
        handleSearch={handleSearch}
        handleSearchSubmit={handleSearchSubmit}
      />
      <div className="books-lists">
        <SearchList
          handleAddMyBook={handleAddMyBook}
          books={books}
        />
        <MyList
          myBooks={myBooks}
          handleRemoveMyBook={handleRemoveMyBook}
        />
      </div>
    </div>
  )
}

export default Books;