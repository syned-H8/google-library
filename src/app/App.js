import './App.css';
import Header from './components/header';
import Books from './components/books';

function App() {
  return (
    <div className="App">
      <Header />
      <Books />
    </div>
  );
}

export default App;
